package com.example.mongodb.repository;

import com.example.mongodb.document.Account;
import com.example.mongodb.dto.AccountDataDTO;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

import java.util.List;

@Repository
public interface AccountCrudRepository extends ReactiveCrudRepository<Account, String> {

    @Query(value="{}", fields="{data : 1, _id : 0}")
    Flux<AccountDataDTO> findDataAndExcludeId();

    @Query(value="{}", fields="{data : 1, roles : 1, _id : 0}")
    Flux<Account> findDataAndRolesAndExcludeId();
}

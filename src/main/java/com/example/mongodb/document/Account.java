package com.example.mongodb.document;

import com.example.mongodb.document.model.Role;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Document
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Account {

    @Id
    private String id;

    private double schema_version = 2.0;

    private String data;

    private List<Role> roles = new ArrayList<>();

    public Account(String id, String data) {
        this.id = id;
        this.data = data;
    }
}

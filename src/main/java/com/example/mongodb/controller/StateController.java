package com.example.mongodb.controller;

import com.example.mongodb.document.State;
import com.example.mongodb.dto.StatePopulation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.aggregation.*;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.*;

@RestController
public class StateController {

    @Autowired
    private ReactiveMongoTemplate template;

    @PostMapping("/state")
    public Mono<String> saveState(@RequestBody State state){
        return template.save(state).then(Mono.just("Success"));
    }

    @PostMapping("/states")
    public Mono<String> saveStates(@RequestBody List<State> states){
        return Flux.fromIterable(states).flatMap(template::save).then(Mono.just("Success"));
    }

    @GetMapping("/states/population")
    public  Flux<StatePopulation> statesPopulation(){
        GroupOperation groupByStateAndSumPop = group("state")
                .sum("population").as("statePop");
        MatchOperation filterStates = match(new Criteria("statePop").gt(101));
        SortOperation sortByPopDesc = sort(Sort.by(Sort.Direction.DESC, "statePop"));

        Aggregation aggregation = newAggregation(
                groupByStateAndSumPop, filterStates, sortByPopDesc);
        return template.aggregate(aggregation,"state", StatePopulation.class);
    }
}

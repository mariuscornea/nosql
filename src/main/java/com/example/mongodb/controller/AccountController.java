package com.example.mongodb.controller;

import com.example.mongodb.document.Account;
import com.example.mongodb.document.model.Role;
import com.example.mongodb.dto.AccountDataDTO;
import com.example.mongodb.repository.AccountCrudRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

@RestController
public class AccountController {

    @Autowired
    public AccountCrudRepository accountCrudRepository;

    @Autowired
    private ReactiveMongoTemplate template;

    @PostMapping("/accounts")
    public Mono<String> saveAccount(@RequestBody Account account) {
        return template.save(account).then(Mono.just("Success"));
    }

    @GetMapping("/test")
    public Mono<String> test() {
        return accountCrudRepository.save(new Account(UUID.randomUUID().toString(), "asd"))
                .then(Mono.just("Success"));
    }

    @GetMapping("/testt")
    public Mono<String> testt() {
        return template.save(new Account(UUID.randomUUID().toString(), "asd"))
                .then(Mono.just("Success"));
    }

    @GetMapping("/accounts")
    public Flux<Account> getAllAccounts() {
        return accountCrudRepository.findAll();
    }

    @GetMapping("/test2")
    public Flux<Account> test2() {
        Query query = new Query();
        query.fields().include("data").exclude("id");
        return template.find(query, Account.class);
    }

    @GetMapping("/accounts/exclude-id")
    public Flux<AccountDataDTO> accountExcludeId() {
        return accountCrudRepository.findDataAndExcludeId();
    }

    @GetMapping("/accounts/data-and-roles-with-exclude-id")
    public Flux<Account> accountWithDataAndRolesExcludeId() {
        return accountCrudRepository.findDataAndRolesAndExcludeId();
    }

    @GetMapping("/accounts/template/exclude-id")
    public Flux<AccountDataDTO> accountExcludeIdemplate() {
        Query query = new Query();
        query.fields().include("data").exclude("id");
        return template.find(query, AccountDataDTO.class, "account");
    }

    @GetMapping("/accounts/template/exclude-id-with-role")
    public Flux<Account> accountExcludeIdWithRoleTemplate() {
        Query query = new Query();
        query.fields()
                .include("roles")
                .exclude("id");
        return template.find(query, Account.class, "account");
    }

    @PutMapping("/accounts/{id}")
    public Mono<String> test(@RequestBody Role role, @PathVariable("id") String id) {
        Update update = new Update();
        update.addToSet("roles", role);
        Criteria criteria = Criteria.where("_id").is(id);
        return template.updateFirst(Query.query(criteria), update, "account").then(Mono.just("Success"));
    }
}

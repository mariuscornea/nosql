package com.example.mongodb.dto;

import com.example.mongodb.document.model.Role;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AccountDataWithRolesDTO {

    private String data;
    private List<Role> roles;
}
